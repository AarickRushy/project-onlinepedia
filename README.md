## Tampilan User Interface

**Login**

![Img 1](Dokumentasi/Halaman Login.JPG)

**Dashboard**

![Img 2](Dokumentasi/Halaman Dashboard.JPG)

**Profile**

![Img 3](Dokumentasi/Halaman Profile.JPG)

**Data Kategori**

![Img 4](Dokumentasi/Halaman Data Kategori.JPG)

**Data Produk**

![Img 5](Dokumentasi/Halaman Data Produk.JPG)
